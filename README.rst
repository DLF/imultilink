==========
imultilink
==========

i(nteractive)multilink is a simple python script to sym-link multiple files.

I use it to link multiple images from within sxiv.

Dependencies
============
imultilink uses several other programs. Beside `/usr/bin/sh` and `ln`,
imultilink uses `kdialog`, `notify-send`, and `kitty` and `vim` as the editor.

However, those programs can be changed on the top of the script.


Usage
=====
One or more files are provided line-wise via stdin to the script.

The script them opens a dialog to choose a directory.

If no target file names do already exist and if no file name appears twice,
a yes/no-dialog pops up and asks if the links shall be created.

If there is a name conflict or if one of the targets already exist,
and editor is opened where the link commands can be edited, for example
to change the target file names or to add a --force switch to `ln`.
Afterwards, the script is executed.


Installation
============
Just put imultilink somewhere in the path and make it executable.


+--------------------------------------------------------------------------------------+
| imultilink is a free and open project, you can redistribute it and/or modify         |
| it under the terms of the `GNU General Public License`_ as published by              |
| the Free Software Foundation, either version 3 of the License, or any later version. |
|                                                                                      |
| imultilink is distributed in the hope that it will be useful,                          |
| but without any warranty; without even the implied warranty of                       |
| merchantability or fitness for a particular purpose.  See the                        |
| GNU General Public License for more details.                                         |
+--------------------------------------------------------------------------------------+

.. _GNU General Public License: http://www.gnu.org/licenses/
