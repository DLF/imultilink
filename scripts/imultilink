#!/usr/bin/env python

import os
import re
import sys
import tempfile
import unittest

from subprocess import Popen, PIPE

get_dir_call = [
    #"kdialog", "--getexistingdirectory", "--title", "\"Where to link {num} files to?\""
    "kitty", "bash", "-c", "~/.local/bin/joshuto --output-file /tmp/imultilink-joshuto;"
]
get_dir_call_2 = [
    #"kdialog", "--getexistingdirectory", "--title", "\"Where to link {num} files to?\""
    "bash", "-c", "head -1 /tmp/imultilink-joshuto;"
]
confirm_link_call = [
    "kdialog", "--title", "Create symlinks?", "--yesno", "Symlink these {num} to {target} now?\n{bullet_list}"
]
editor_call = [
    "kitty", "nvim", "{file}"
]
execute_link_script_call = [
    "/usr/bin/sh", "{file}"
]

notify_error_call = [
    "notify-send", "-u", "critical", "{header}", "{body}"
]

notify_success_call = [
    "notify-send", "{header}", "{body}"
]

reject_values = ["no", "No", "false", "False", "cancel", "Cancel"]


class LinkList(list):

    def __init__(self):
        list.__init__(self)
        self.target_dir = None
        self.script_name = None
        self._link_elements = None

    def assert_src_elements(self):
        # also asserts existence of source files/dirs by LinkElement’s constructor
        assert len(self.link_elements) > 0, "No files/directories provided."

    @property
    def link_elements(self):
        for s in self:
            assert os.path.exists(s), "File/directory {} does not exists. Aborting.".format(s)
        if self._link_elements is None:
            self._link_elements = [LinkElement(s, self.target_dir) for s in self]
        return self._link_elements

    @property
    def has_duplicates(self):
        return len(self) != len(set(self))

    @property
    def has_conflicts(self):
        assert self.target_dir is not None
        for e in self.link_elements:
            if e.target_exists:
                return True
        return False

    @property
    def bullet_list(self):
        return "\n".join(["• " + s for s in self])

    def write_link_script(self):
        tf = tempfile.NamedTemporaryFile(mode='w+', delete=False)
        self.script_name = tf.name
        print(self.script_name)
        tf.write("#!/usr/bin/sh\n\n")
        tf.write("# {} files/directories to be linked.\n".format(len(self)))
        tf.write("# Clean the file to abort.\n\n".format(len(self)))
        script_has_duplicates = self.has_duplicates
        script_has_conflicts = self.has_conflicts
        if script_has_duplicates:
            tf.write("# ! Has duplicates !\n")
        if script_has_conflicts:
            tf.write("# ! Has conflicts !\n")
        if script_has_conflicts or script_has_duplicates:
            tf.write("# ! This script needs to be fixed manually !\n\n")
        else:
            tf.write("# No conflicts or duplicates. :)\n\n")
        used_targets = set()
        max_src = max([len(e.src_file) for e in self.link_elements])
        max_dest = max([len(e.target) for e in self.link_elements])
        digits_of_number = len(str(len(self)))
        for ix, e in enumerate(self.link_elements):
            file_has_duplicate = e.target in used_targets
            file_has_conflict = e.target_exists
            comment = ""
            if file_has_duplicate:
                comment += "DUPLICATE"
            if file_has_conflict:
                if len(comment) > 0:
                    comment += " & "
                comment += "CONFLICT"
            if len(comment) > 0:
                comment = "   # " + comment
            tf.write("ln -sr {src:<{src_w}}  {dest:<{dest_w}}  ||  exit $(({code:>{code_w}} + $?)){comment}\n".format(
                src='"' + e.src_file + '"',
                dest='"' + e.target + '"',
                src_w=max_src + 2,
                dest_w=max_dest + 2,
                code=(ix+1) * 1000,
                code_w=digits_of_number + 3,
                comment=comment
            ))
            used_targets.add(e.target)

        tf.write("\n\n# --- end ---\n")
        tf.close()
        return self.script_name


class LinkElement:
    def __init__(self, src_file, target_dir):
        self.src_file = src_file
        self.target_name = os.path.basename(os.path.normpath(src_file))
        self.target_dir = target_dir
        target_name_parts = self.target_name.split('.')
        if len(target_name_parts) <= 1:
            file_name = self.target_name
        else:
            file_name = '.'.join(target_name_parts[:-1])
        if re.match(r"\d*$", file_name):
            # file name consists only out of numbers
            abs_path = os.path.abspath(src_file)
            _, last = os.path.split(os.path.dirname(abs_path))
            self.target_name = last+self.target_name
        self.target = os.path.join(self.target_dir, self.target_name)

    @property
    def target_exists(self):
        return os.path.exists(self.target)


class FinalError(Exception):
    def __init__(self, header, body):
        self.header = header
        self.body = body


def call(template, var_dict):
    _call = [e.format(**var_dict) for e in template]
    print(_call)
    process = Popen(_call, stdout=PIPE, stderr=PIPE)
    stdout, stderr = process.communicate()
    exit_code = process.wait()
    print(stdout)
    print(stderr)
    stdout = "" if stdout is None else stdout.decode("utf-8").strip()
    stderr = "" if stderr is None else stderr.decode("utf-8").strip()
    return exit_code, stdout,stderr


def main():

    elements = LinkList()
    for line in sys.stdin:
        elements.append(line.strip())

    info_dict = {
        "num": len(elements),
        "bullet_list": elements.bullet_list
    }

    exit_code, stdout, stderr = call(get_dir_call, info_dict)
    if exit_code != 0:
        print("get_dir_call returned {}. Aborting. Stderr: \"{}\"".format(exit_code, stderr), file=sys.stderr)
        return
    if len(get_dir_call_2) > 0:
        exit_code, stdout, stderr = call(get_dir_call_2, info_dict)
        if exit_code != 0:
            print("get_dir_call_2 returned {}. Aborting. Stderr: \"{}\"".format(exit_code, stderr), file=sys.stderr)
            return

    target_dir = stdout
    info_dict["target"] = target_dir

    if not os.path.isdir(target_dir):
        raise FinalError("Target directory doesn’t exist.", "Target directory {} does not exist.".format(target_dir))

    elements.target_dir = target_dir

    file_name = elements.write_link_script()
    info_dict["file"] = file_name

    if elements.has_duplicates or elements.has_conflicts:
        exit_code, stdout, stderr = call(editor_call, info_dict)
        if exit_code != 0:
            raise FinalError(
                "Editor returned error.", "editor_call returned {}.\nStderr:\n{}".format(
                    exit_code, stderr
                )
            )
    else:
        exit_code, stdout, stderr = call(confirm_link_call, info_dict)
        if exit_code != 0 or stdout in reject_values:
            print(
                "Rejected with code {} and stdout \"{}\". Aborting. Stderr: \"{}\"".format(
                    exit_code, stdout, stderr
                ), file=sys.stderr
            )
            return

    exit_code, stdout, stderr = call(execute_link_script_call, info_dict)

    if exit_code != 0:
        raise FinalError(
            "Link script execution failed!", "Link script {} returned {}.\nStderr:\n{}".format(
                file_name, exit_code, stderr
            )
        )

    h = "Link script executed."
    b = "Link script {} executed without errors.".format(file_name)
    print(b)
    call(notify_success_call, {"header": h, "body": b})


if __name__ == "__main__":
    try:
        main()
    except FinalError as e:
        print("{} Aborting.\n{}".format(e.header, e.body), file=sys.stderr)
        call(notify_error_call, {"header": e.header, "body": e.body})


class LinkElementTest(unittest.TestCase):

    def test_only_number_file_name_is_extended_by_lowest_path(self):
        le = LinkElement(
            src_file="/foo/bar/123",
            target_dir="/target/dir/"
        )
        self.assertEqual(le.target_name, "bar123")

    def test_number_plus_suffix_file_name_is_extended_by_lowest_path(self):
        le = LinkElement(
            src_file="/foo/bar/123.py",
            target_dir="/target/dir/"
        )
        self.assertEqual(le.target_name, "bar123.py")

    def test_only_number_file_name_on_root_dir_is_left_unchanged(self):
        le = LinkElement(
            src_file="/123",
            target_dir="/target/dir/"
        )
        self.assertEqual(le.target_name, "123")
